<?php
require_once '__core.php';

$user = new User();

$errors = "";

if(!$user->isLoggedIn())
    Header::Redirect("/404.php");

do if(Input::isType("POST")){

    if(!Token::check(Input::get("token"))){
        $errors .= "<p>CSRF detected</p>";
        break;
    }

    
    do if(Input::exists("Email")){
        $validation = new Validation($_POST, array(
            "Email" => array(
                "required" => true,
                "unique" => "users"
            )
        ));
        if(!$validation->passed()){
            $errors .= implode("<br />", $validation->getErrors()) . "<br />";
            break 1;
        }
    
        $errors .= (!$user->changeEmail(Input::get("Email"))) ? "<p>There was a problem with changing your email address.</p>" : "<p>Email successfully changed.</p>";
    } while (false);

    do if(Input::exists("Password")){
        $validation = new Validation($_POST, array(
            "Password" => array(
                "required" => true,
                "min_length" => 7
            )
        ));

        if(!$validation->passed()){
            $errors .= implode("<br />", $validation->getErrors()). "<br />";
            break 1;
        }
        $salt = Hash::salt(64);
        $password = Hash::make(Input::get("Password"), $salt);
    
        $errors .= ($user->changePassword($password, $salt)) ? "<p>Password successfully changed.</p>" : "<p>There was a problem with changing your password.</p>";

    } while(false);


    do if(Input::exists("Description")){
        
        $errors .= ($user->ChangeDescription(htmlspecialchars(Input::get("Description")))) ? "<p>Description updated. </p>" : "<p>There was a problem updating description.</p>";

    } while(false);


} while (false);

$template = new Template("account");
$template->add("navbar", $template->MakeNavbar());
$template->add("sidebar", $template->MakeSidebar());

$template->add("errors", $errors);
$template->add("token", Token::generate());

$template->add("email", $user->getData()->email);
$template->add("description", $user->getData()->description);
$template->render();