<?php
require_once '__core.php';

$user = new User();
if($user->isLoggedIn()){
  Header::Redirect('/');
}

$RegisterError = "";

do if(Input::isType("POST")){
  if(!Token::check(Input::get("token"))){
    $RegisterError = "CSRF detected!";
    break;
  }

  $validation = new Validation(Input::getData(), [

    "Username" => [
      "required" => true,
      "min_length" => 4,
      "max_length" => 20,
      "unique" => "users"
    ],
    "Password" => [
      "required" => true,
      "min_length" => 7
    ],
    "Email" => [
      "required" => true,
      "unique" => "users",
      "filter" => FILTER_VALIDATE_EMAIL
    ]

  ]);

  if(!$validation->passed()){
    foreach($validation->getErrors() as $error){
      $RegisterError .= "<p>{$error}</p>";
    }
    break;
  }

  $salt = Hash::salt(64);

  if(!$user->register([
    "username" => Input::get("Username"),
    "password" => Hash::make(Input::get("Password"), $salt),
    "salt" => $salt,
    "email" => Input::get("Email"),
    "ip" => $_SERVER['REMOTE_ADDR']
  ])){
    $RegisterError = "Something went wrong, please try again.";
    break;
  }

  Session::flash("register", "Registration successful, you can log-in.");
  Header::Redirect("/");

} while(false);

$template = new Template("register");
$template->add("navbar", $template->MakeNavbar());
$template->add("sidebar", $template->MakeSidebar());

$template->add("RegisterError", $RegisterError);


$template->add("token", Token::generate());

$template->render();
