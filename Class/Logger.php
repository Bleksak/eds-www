<?php
class Logger {

  const FATAL_ERROR = 3;
  const WARNING =     2;
  const INFO =        1;
  const MESSAGE =     0;

  private static $file;
  public static function Log($message, $action = 0){
    self::$file = new File("Logs/".time().".txt");


    switch ($action) {
      case self::FATAL_ERROR:
        self::$file->Write("[FATAL_ERROR]: ". $message);
        die("Fatal error");
        break;
      case self::WARNING:
        self::$file->Write("[WARNING]: " . $message);
        echo "Notice: {$message}";
        break;
      case self::INFO:
        self::$file->Write("[INFO]: " . $message);
        break;
      case self::MESSAGE:
        self::$file->Write("[MESSAGE]: ". $message);
        break;
      default:
        self::$file->Write("[UNKNOWN]: ". $message);
        break;
    }

    return true;
  }
}
