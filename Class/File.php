<?php
class File {

  private $filename;

  private $f;

  public function __construct($filename){
    $this->filename = $filename;

    $this->f = fopen($filename, "a+") or die("File {$filename} couldn't be opened.");

  }

  public function __destruct(){
    @fclose($this->f);
  }

  public function Read(){
    return (filesize($this->filename)) == 0 ? "" : fread($this->f, filesize($this->filename));
  }

  public function Write($string){
    return (bool) fwrite($this->f, $string);
  }

  public static function CreateFile($filename){
    $f = fopen($filename, 'c');
    fclose($f);
  }
  public static function DeleteFile($filename){
    if(!is_file($filename))
      return false;
    return unlink($filename);
  }
}
