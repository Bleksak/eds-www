<?php
class Session {

  public static function exists($name){
    return !empty($_SESSION[$name]);
  }

  public static function get($name){
    if(self::exists($name))
      return $_SESSION[$name];

    return null;
  }

  public static function set($name, $value){
    return $_SESSION[$name] = $value;
  }

  public static function delete($name){
    unset($_SESSION[$name]);
  }

  public static function flash($name, $value=''){
    if(self::exists($name)){
      $ret = self::get($name);
      self::delete($name);
      return $ret;
    }
    if(empty($value))
      return false;

    return self::set($name, $value);
  }
}
