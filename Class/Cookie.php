<?php
class Cookie {

  public static function exists($name){
    return isset($_COOKIE[$name]);
  }

  public static function get($name){
    if(self::exists($name))
      return $_COOKIE[$name];

    return null;
  }

  public static function set($name, $value, $time = 7 * 24 * 60 * 60){
    return setcookie($name, $value, time() + $time);
  }

  public static function delete($name){
    return self::set($name, null, -1);
  }
}
