<?php
class Validation {

  private $_passed = false,
  $_errors = array(),
  $_db = null;

  public function __construct($data, $validation){
    $this->_db = Database::getInstance();

    foreach($validation as $item => $rules){

      $field = $data[$item];

      foreach($rules as $rule=>$rule_value){
        if($rule == 'required' && empty($field))
          $this->_errors[] = "{$item} cannot be empty.";

        else {
          switch($rule) {
            case 'min_length':

              if(strlen($field) < $rule_value)
                $this->_errors[] = "{$item} must be at least {$rule_value} characters.";

              break;

            case 'max_length':

              if(strlen($field) > $rule_value)
                $this->_errors[] = "{$item} cannot be longer than {$rule_value} characters.";

              break;

            case 'unique':

              if($this->_db->get($rule_value, array("LOWER(".$item.')', "=", strtolower($field)))->getCount() > 0)
                  $this->_errors[] = "{$item} is already taken.";
              break;

            case 'filter':

              if(!filter_var($field, $rule_value))
                $this->_errors[] = "Invalid {$item} format";

            break;
            }
          }
        }
      }
    $this->_passed = empty($this->_errors);

  }

  public function passed(){
    return $this->_passed;
  }

  public function getErrors(){
    return $this->_errors;
  }
}
