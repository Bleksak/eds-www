<?php
class Config {
  public static function get($key){

    $config = $GLOBALS["config"];

    if(!is_array($config)){
      return Logger::Log("\$GLOBALS[\"Config\"] is not an array!", Logger::FATAL_ERROR);
    }

    $keyarray = explode('/', $key);

    for($i = 0; $i<sizeof($keyarray); $i++){
      if(!array_key_exists($keyarray[$i], $config)){
        return Logger::Log("Config " . $key . "[" . $keyarray[$i] . "] wasn't found.", Logger::FATAL_ERROR);
      }
      $config = $config[$keyarray[$i]];
    }
    return $config;
  }
}
