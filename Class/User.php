<?php
class User {

  private $_id = null,
          $_data = null,
          $_isLoggedIn = false,
          $_db = null,
          $_isAdmin = 0;

  public function __construct($user=null){
    $this->_db = Database::getInstance();

    if(Session::exists("user") && $user == null)
      $this->login(Session::get("user"));

    if($user){
      $this->find($user);
    }
  }

  public function login($username, $password=null, $remember = false){
    if($password == NULL){
      if(!$this->find($username))
        return false;

      $this->_data = $this->_db->getResult()[0];
      if($this->_data->banned){
        Session::flash("banned", "Account {$this->_data->username} has been banned.");
        return false;
      }
      $this->_isLoggedIn = true;
      $this->_id = (int)$username;
      $this->_isAdmin = $this->_data->admin;

      if(!Session::exists("user"))
        Session::set("user", $this->_id);

      return true;
    }

    $this->_db->get("users", array("LOWER(Username)","=", strtolower($username)));

    if($this->_db->getCount() == 0)
      return false;

    $this->_data = $this->_db->getResult()[0];

    if($this->_data->password !== Hash::make($password, $this->_data->salt))
      return false;

    if($this->_data->banned){
      Session::flash("banned", "Account {$this->_data->username} has been banned.");
      return false;
    }

    $this->_id = $this->getData()->id;
    $this->_isAdmin = $this->_data->admin;

    $this->_isLoggedIn = true;
    Session::set("user", $this->_data->id);

    if($remember == true){

      $cookie = md5(uniqid());

      $this->_db->insert("user_remember", array(

        "user_id" => $this->_id,
        "CookieID" => $cookie

      ));

      Cookie::set("user_remember", $cookie);
    }

    return true;
  }

  public function find($id = null){
    $params = ($id == null) ? array() : array('id', '=', $id);
    if($this->_db->get("users", $params)->getCount() == 0)
      return false;

    $this->_data = ($id!=null) ? $this->_db->getResult()[0] : $this->_db->getResult();

    if($id!= null){
      $this->_id = $this->_data->id;
      $this->_isAdmin = $this->_data->admin;
    }

    return $this->_db->getCount();
  }

  public function register($data){
    if(sizeof($data) != $this->_db->query("SELECT count(*) FROM information_schema.columns WHERE table_name = ? AND table_schema = ?", array("users", Config::get("mysql/database")))->getResult()[0]->{"count(*)"} - 4)
      return false;


    $data['register_date'] = date("Y-m-d");
    $data['banned'] = 0;
    $data['admin'] = 0;
    return !$this->_db->insert("users", $data)->getError();
  }

  public function logout(){
    if(!$this->isLoggedIn())
      return false;

    if(Cookie::exists("user_remember")){
      $this->_db->delete("user_remember", array("user_id", '=', $this->_id));
      Cookie::delete("user_remember");
    }

    Session::delete("user");
    return true;
  }

  public function ToggleMod($id){
    if(!$this->find($id))
      return false;

    $err = !$this->_db->update("users", array(
      'admin' => (int)!(bool)($this->_data->admin)
    ), array("id", '=', $id))->getError();

    $this->find($this->_id);

    return !$this->_db->getError();
  }

  public function ToggleBan($id){
    if(!$this->find($id))
      return false;

    $err = !$this->_db->update("users", array(
      'banned' => (int)!(bool)($this->_data->banned)
    ), array("id", '=', $id))->getError();

    $this->find($this->_id);

    return $err;
  }

  public function delete($id){
    if(!$this->find($id))
      return false;

    $err = !$this->_db->delete("users", array("id", '=', $id))->getError();
    $this->find($this->_id);

    return $err;
  }

  public function ChangeUsername($username){
    if($username == $this->_data->username)
      return true;

    if((bool)$this->_db->get("users", array("LOWER(username)", '=', strtolower($username)))->getCount())
      return false;

    $err = !$this->_db->update("users", array(
      "username" => $username
    ), array("id", '=', $this->_id))->getError();

    $this->find($this->_id);

    return $err;
  }

  public function ChangeEmail($email){
    if($email == $this->_data->email)
      return true;

    if((bool)$this->_db->get("users", array("LOWER(email)", '=', strtolower($email)))->getCount())
      return false;

    $err = !$this->_db->update("users", array(
      "email" => $email
    ), array("id", '=', $this->_id))->getError();

    $this->find($this->_id);

    return $err;
  }

  public function ChangePassword($password, $salt){
    $err = !$this->_db->update("users", array(
      "password" => $password,
      "salt" => $salt
    ), array("id", '=', $this->_id))->getError();

    $this->find($this->_id);

    return $err;
  }

  public function ChangeDescription($description){
    $err = !$this->_db->update("users", array(
      "description" => $description
    ), array("id", '=', $this->_id))->getError();

    $this->find($this->_id);

    return $err;
  }

  public function isAdmin(){
    return $this->_isAdmin;
  }

  public function getData(){
    return $this->_data;
  }

  public function isLoggedIn(){
    return $this->_isLoggedIn;
  }

  public function isBanned(){
    return $this->_data->banned;
  }
}
