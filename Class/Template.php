<?php
class Template extends Template_ext {
  private $_db;

  public function __construct_ext(){
    $this->_db = Database::getInstance();
  }

  public function MakeSidebar(){
    $user = new User();
    return ($user->isLoggedIn()) ? (string) new Template("sidebar_loggedin") : (string) new Template("sidebar");
    //NOTE: Make sure not to loop in documents
  }

  public function MakeNavbar(){
    $user = new User();
    return ($user->isLoggedIn()) ? (string) new Template("navbar_loggedin") : (string) new Template("navbar");
  }

  public function MakeComments($id){
    $articles = new Articles();
    if(!$articles->find($id))
      return "<h3>There are no comments.</h3>";
  
    $html = "<h1>Comments: </h1>";

    $user = new User();

    foreach($articles->getComments() as $comment){
      $user->find($comment->userid);
      $html .= "<h4><a href='user.php?id={$comment->userid}'>" . $user->getData()->username . "</a></h4>";
      $html .= "<p>" . $comment->content . "</p>";
    }

    return $html;
  }

  public static function MakeArticles($id = null, $page = null){
    $articles = new Articles();

    if(!$articles->find($id, Articles::CalculateLimit($page)))
      return ((bool)$id || $page>1 || $page<0) ? Header::Redirect("/404.php") : "<p>There are no articles at the moment.</p>";
    elseif((bool)$id) return "<h1>{$articles->getResult()->name}</h1>\n{$articles->getResult()->content}";

    $html = "";

    foreach($articles->getResult() as $article){
      $html .= "<h1><a href='/article.php?id={$article->id}'>{$article->name}</a></h1>";
      $html .= $article->content;
    }
    
    return $html;
  }

  public function MakeProjects($id = null, $page = null){
    $projects = new Projects();

    if(!$projects->find($id, Articles::CalculateLimit($page)))
      return ((bool)$id || $page>1 || $page<0) ? Header::Redirect("/404.php") : "<p>There are no projects at the moment.</p>";
    elseif((bool)$id) return "<h1>{$projects->getResult()->name}</h1>\n{$projects->getResult()->content}";

    $html = "";

    foreach($projects->getResult() as $project){
      $html .= "<h1><a href='/project.php?id={$project->id}'>{$project->name}</a></h1>";
      $html .= $project->content;
    }
    
    return $html;
  }

  public function CreateProject($param){
    $id = $this->parseFunctionCall($param)[0];
    if($this->_db->get("projects", array("id", "=", $id))->getCount() == 0)
      Header::Redirect('404.php');

    $html = "<h1>" . $this->_db->getResult()[0]->name . "</h1>";
    $html .= $this->_db->getResult()[0]->description;

    return $html;
  }
}
