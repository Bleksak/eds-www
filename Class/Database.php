<?php
class Database {
  private static $instance;

  private $_pdo, $_stmt, $_result, $_error, $_count;

  public static function getInstance(){
    return (isset(self::$instance)) ? self::$instance : self::$instance = new Database();
  }

  private function __construct(){
    try {

      $this->_pdo = new PDO("mysql:dbname=".Config::get("mysql/database").";host=".Config::get("mysql/hostname").";charset=UTF8", Config::get("mysql/username"), Config::get("mysql/password"));

    } catch(PDOException $e){
      Logger::Log("PDOException caught, ". $e->getMessage(), Logger::FATAL_ERROR);
    }
  }

  public function query($statement, $values = array()){
    $this->_stmt = $this->_pdo->prepare($statement);
    if(!empty($values)){

      if(substr_count($statement, '?') != sizeof($values))
        Logger::Log("Database error: Question mark count does not equal the count of values", Logger::FATAL_ERROR);

      foreach($values as $key=>$value){
        $this->_stmt->bindValue($key+1, $value);
      }
    }

    if(!$this->_stmt->execute()) {
      $this->_error = true;
      $this->_count = 0;
      $this->_result = null;
    } else {

      $this->_error = false;
      $this->_count = $this->_stmt->rowCount();
      $this->_result = $this->_stmt->fetchAll(PDO::FETCH_OBJ);

    }
    return $this;
  }

  public function action($action, $table, $where = array(1, '=', 1), $special = ''){
    if(empty($where)){
      $where = [1, '=', 1];
    }

    $name = $where[0];
    $operator = $where[1];
    $value = $where[2];

    $operators = ['<', '=', '>', '=<', '>='];
    if(!in_array($operator, $operators))
      Logger::Log("Database error: unsupported operator {$operator} used in a statement.", Logger::FATAL_ERROR);


    $statement = "{$action} FROM {$table} WHERE {$name} {$operator} ?  {$special}";

    return $this->query($statement, array($value));
  }

  public function get($table, $where = array(), $special = ''){
    return $this->action("SELECT *", $table, $where, $special);
  }

  public function delete($table, $where){
    return $this->action("DELETE", $table, $where);
  }

  public function insert($table, $params){
    $keys = array_keys($params);
    $values = array_values($params);

    $keys = implode(',', array_map(function($key){
      return '`'.$key.'`';
    }, $keys));

    $question = "?";

    for($i = sizeof($values); $i!= 1; $i--){
      $question .= ", ?";
    }

    $statement = "INSERT INTO {$table} ($keys) VALUES ($question)";

    return $this->query($statement, $values);

  }

  public function update($table, $params, $where = array(1, '=', 1)){

    $keys = array_keys($params);
    $values = array_values($params);
    $values[] = $where[2];

    $set = "";

    $n = 1;

    foreach($params as $key=>$value){
      $set .= "{$key} = ?";
      if($n!=sizeof($params))
        $set .= ", ";
      $n++;
    }

    $statement = "UPDATE {$table} SET {$set} WHERE {$where[0]} {$where[1]} ?";
    return $this->query($statement, $values);
  }

  public function getError(){
    return $this->_error;
  }
  public function getResult(){
    return $this->_result;
  }
  public function getCount(){
    return $this->_count;
  }
}
