<?php
class Token {
  public static function generate(){
    return Session::set(Config::get("token_name"), md5(uniqid()));
  }

  public static function check($token){
    if(!Session::exists( Config::get("token_name") ))
      return false;

    return Session::flash(Config::get("token_name")) === $token;
  }
}
