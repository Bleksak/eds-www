<?php
class Header {
  public static function Redirect($location){
    header("Location: {$location}") & die();
  }

  public static function NotFound(){
    header("HTTP/1.0 404 Not Found") & die();
  }

}
