<?php
class Template_ext {
  private $_template = null,
          $_file_cont = null,
          $_vars = array();

  public function __construct($template){
    $this->_template = $template;
    $this->_file_cont = new File(Config::get("template_folder") . $template . ".html");
    $this->_file_cont = $this->_file_cont->Read();
  }

  public function __toString(){
    return $this->_file_cont;
  }

  public function add($name, $value){
    $this->_vars[$name] = $value;
  }

  public function render(){
    foreach($this->_vars as $key=>$value){
      $this->_file_cont = preg_replace("/\[" . $key . "\]/", $value, $this->_file_cont);
    }

    echo $this;
  }
}
