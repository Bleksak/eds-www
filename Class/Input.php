<?php
class Input {

  public static function get($name){

    if(self::exists($name))
      return $GLOBALS['_'.$_SERVER['REQUEST_METHOD']][$name];
    return null;
  }

  public static function exists($name){
    return !empty($GLOBALS['_'.$_SERVER['REQUEST_METHOD']][$name]);
  }

  public static function isType($type){
    return $_SERVER['REQUEST_METHOD'] == $type;
  }

  public static function getData(){
    return $GLOBALS['_'.$_SERVER['REQUEST_METHOD']];
  }
}
