<?php
class Articles {
  protected $_id = null, $_articles = array(), $_db, $_count = null, $dbTable, $_comments = array();

  public function __construct($id = null, $dbTable = null){
    $this->_db = Database::getInstance();

    $this->dbTable = ($dbTable!=null) ? $dbTable : strtolower(__CLASS__);

    $this->_count = $this->_db->query("SELECT count(*) FROM {$this->dbTable}")->getResult()[0]->{"count(*)"};
    $this->_id = $id;
    $this->find($id);

  }

  public function find($id=null, $limit = null){

    $params = ($id == null) ? array() : array("id", '=', $id);

    $this->_db->get($this->dbTable, $params, "ORDER BY ID DESC ".$limit);
  
    if(!(bool)$this->_db->getCount()){
      $this->_articles = null;
      return false;
    }

    $this->_articles = ($id != null) ? $this->_db->getResult()[0] : $this->_db->getResult();
    $this->FindComments();
    return true;
  }


  private function FindComments(){
    if($this->_articles == null || is_array($this->_articles)) 
      return false;

    $this->_comments = $this->_db->get($this->dbTable."_comments", array("articleid", '=', $this->_articles->id), "ORDER BY ID DESC")->getResult();
  }

  public function getComments(){
    return $this->_comments;
  }

  public function addComment($content){
    $user = new User();
    if(!$user->isLoggedIn())
      return false;
    
    $err = !$this->_db->insert($this->dbTable."_comments", array(
      "articleid" => $this->getResult()->id,
      "userid" => $user->getData()->id,
      "content" => $content
    ))->getError();

    $this->find($this->_id);

    return $err;
  }

  public function RemoveComment($id){
    
    $err = !$this->_db->delete($this->dbTable."_comments", array("id", '=', $id))->getError();
    $this->find($this->_id);

    return $err;
  }

  public function create($name, $content){
    $user = new User();

    $err =  !$this->_db->insert($this->dbTable, array(
      "name" => $name,
      "content" => $content,
      "author" => $user->getData()->username,
      "date_added" => date("Y-m-d")
      ))->getError();

      $this->find($this->_id);

      return $err;
    }

    public function delete($id){
      $err = !$this->_db->delete($this->dbTable, array("id", '=', $id))->getError();
      $this->_db->delete($this->dbTable."_comments", array("articleid", '=', $id));

      $this->find($this->_id);

      return $err;
    }
    
    public function edit($id, $name, $content){
      $err = !$this->_db->update($this->dbTable, array(
        "name" => $name,
        "content" => $content
      ), array("id", '=', $id))->getError();

      $this->find($this->_id);

      return $err;
    }

    public static function CalculateLimit($page = null){

      if(Config::get("articles_per_page") == 0){
        return "";
      }

      if($page == null || $page == 0)
        $page = 1;

      if($page<1)
        Header::Redirect("/404.php");
      

      $start = ($page-1) * Config::get("articles_per_page");
      $end = Config::get("articles_per_page");

      return "LIMIT {$start}, {$end}";

    }

    public function GetCurrentPage(){

      if(!Input::isType("GET") || !Input::exists(Config::get("paging")) || Input::get(Config::get("paging")) < 2 || $this->count() < (Input::get(Config::get("paging"))-1) * Config::get("articles_per_page"))
        return 1;

      return Input::get(Config::get("paging"));

    }

    public function GetStart(){
      return ($this->count() == 0) ? 0 : ($this->GetCurrentPage()-1) * Config::get("articles_per_page") +1;

    }

    public function GetEnd(){
      return ($this->count() < $this->GetStart() + Config::get("articles_per_page")) ? $this->count() : $this->GetStart() + Config::get("articles_per_page") -1;
    }

    public function GetNumberOfPages(){
      return ($this->count() == 0) ? 1 : ceil($this->count() / Config::get("articles_per_page"));
    }

    
    public function GetBeforeCount(){
      return ($this->GetCurrentPage()-1 > Config::get("pages_count")) ? Config::get("pages_count") : $this->GetCurrentPage() -1;
    }
    
    public function GetAfterCount(){
      return ($this->GetNumberOfPages() - $this->GetCurrentPage() > Config::get("pages_count") ) ? Config::get("pages_count")  : $this->GetNumberOfPages() - $this->GetCurrentPage();
    }
    
    public function isNext(){
      return ($this->GetCurrentPage() != $this->GetNumberOfPages());
    }

    public function isPrev(){
      return ($this->GetCurrentPage() != 1);
    }
  
    public function isFirst(){
      return ($this->GetCurrentPage() - $this->GetBeforeCount()) > 1;
    }

    public function isLast(){
      return $this->GetAfterCount() + $this->GetCurrentPage() < $this->GetNumberOfPages();
    }

   public function count(){
     return $this->_count;
   }

    public function getResult(){
      return $this->_articles;
    }
}
