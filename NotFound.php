<?php
require_once '__core.php';

$template = new Template("404");
$template->add("navbar", $template->MakeNavbar());
$template->add("sidebar", $template->MakeSidebar());

$template->render();
