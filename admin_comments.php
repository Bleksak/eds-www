<?php
require_once '__core.php';


$user = new User();
if(!$user->isLoggedIn() || !$user->isAdmin())
    Header::Redirect("/");

if(!Input::exists("id"))
    Header::Redirect("/admin.php");
    
$articles = new Articles(Input::get("id"));

if(Input::exists("del"))
    ($articles->RemoveComment(Input::get("del"))) ? $success = "Successfully deleted." : $error = "Problem deleting."; 



$success = "";
$error = "";

$comment_count = sizeof($articles->getComments());

$comments_html = "";

if($comment_count == 0){

  $comments_html = "<tr><td>There are no comments yet.</td></tr>";

} else foreach($articles->getComments() as $comment){

    $user = new User($comment->userid);


  $comments_html .= "<tr>
      <td><input type=\"checkbox\" class=\"checkbox\" /></td>
      <td><h3>". substr($comment->content, 0, 25) ."</h3></td>
      <td></td>
      <td><a href='#'>{$user->getData()->username}</a></td>
      <td><a href='admin_comments.php?id=". Input::get("id") ."&del={$comment->id}' class=\"ico del\">Delete</a></td>
    </tr>";
}



$success = (!empty($success)) ? "<div class=\"msg msg-ok\">
			<p><strong>{$success}</strong></p>
			<a href=\"#\" class=\"close\">close</a>
		</div>" : "";

$error = (!empty($error)) ? "<div class=\"msg msg-error\">
  <p><strong>{$error}</strong></p>
  <a href=\"#\" class=\"close\">close</a>
</div>" : "";


$template = new Template("admin_comments");
$template->add("success", $success);
$template->add("error", $error);

$template->add("comments", $comments_html);
$template->add("comments_data", $comment_count);

$template->render();