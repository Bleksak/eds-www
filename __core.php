<?php

session_start();

$GLOBALS["config"] = [
  "mysql" => [
    "username" => "root",
    "password" => "",
    "database" => "asdf",
    "hostname" => "127.0.0.1",
    "port" => 3306
  ],

  "paging" => "page",
  "pages_count" => 5,
  "articles_per_page" => 10,

  "token_name" => "token",
  "template_folder" => "templates/"
];


spl_autoload_register(function($class){
  if(class_exists($class))
    return;

  if(!is_file("Class/". $class . ".php")){
    return Logger::Log("Class named " . $class . " couldn't be found.", Logger::FATAL_ERROR);
  }

  require_once "Class/".$class.".php";
});

do if(Cookie::exists("user_remember") && !Session::exists("user")){
  $db = Database::getInstance();
  if($db->get("user_remember", array("CookieID", '=', Cookie::get("user_remember")))->getCount() == 0)
    break;

  $user = new User();
  $user->login($db->getResult()[0]->user_id);
} while (false);
