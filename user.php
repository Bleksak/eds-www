<?php
require_once '__core.php';

$user = new User();
if(!Input::exists('id') && !$user->isLoggedIn())
    Header::Redirect('/404.php');

$template = new Template("user");
$template->add("navbar", $template->MakeNavbar());
$template->add("sidebar", $template->MakeSidebar());
$template->add("username", ucfirst($user->getData()->username));
$template->add("description", nl2br($user->getData()->description));

$template->render();