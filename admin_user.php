<?php
require_once '__core.php';

$users = new User();
if(!$users->isLoggedIn() || !$users->isAdmin())
 Header::Redirect('/');

$error = "";
$success = "";

do if(Input::isType("GET") && Input::exists("ban")){

  if($users->ToggleBan(Input::get("ban"))){
    $success = "User {$users->getData()->username} has been (un)banned.";
    break;
  }

  $error = "There was an error (un)banning user {$users->getData()->username}";

} while (false);

do if(Input::isType("GET") && Input::exists("mod")){

  if($users->ToggleMod(Input::get("mod"))){
    $success = "User {$users->getData()->username} has been (un)modded.";
    break;
  }

  $error = "There was an error (un)modding user {$users->getData()->username}";

} while (false);

do if(Input::isType("GET") && Input::exists("del")){

  if($users->delete(Input::get("del"))){

    $user = new User();
    if(!$user->isLoggedIn())
      Header::Redirect('/');
    $success = "User {$users->getData()->username} has been deleted.";
    break;
  }

  $error = "There was an error deleting user {$users->getData()->username}";

} while (false);

$count = $users->find();

$users_html = "";

foreach($users->getData() as $user){
  $users_html .= "<tr>
      <td><input type=\"checkbox\" class=\"checkbox\" /></td>
      <td><h3><a href='/user.php?id={$user->id}'>{$user->username}</a></h3></td>
      <td>{$user->register_date}</td>
      <td>{$user->email}</td>
      <td><a href='/admin_user.php?ban={$user->id}'>".(($user->banned == 1) ? "YES" : "NO") ."</a></td>
      <td><a href='/admin_user.php?mod={$user->id}'>" .(($user->admin == 1) ? "YES" : "NO"). "</a></td>
      <td><a href='/admin_user.php?del={$user->id}' class=\"ico del\">Delete</a>
      <a href='/admin_user_edit.php?id={$user->id}' class=\"ico edit\">Edit</a></td>
    </tr>";
}




$success = (!empty($success)) ? "<div class=\"msg msg-ok\">
			<p><strong>{$success}</strong></p>
			<a href=\"#\" class=\"close\">close</a>
		</div>" : "";

$error = (!empty($error)) ? "<div class=\"msg msg-error\">
  <p><strong>{$error}</strong></p>
  <a href=\"#\" class=\"close\">close</a>
</div>" : "";

$template = new Template("admin_user");

$template->add("users", $users_html);

$template->add("token", Token::generate());
$template->add("success", $success);
$template->add("error", $error);

$template->render();
