<?php
require_once '__core.php';

$user = new User();
if(!$user->isLoggedIn() || !$user->isAdmin())
 Header::Redirect('/');

$user = new User(Input::get("id"));

 $success = "";
 $error = "";

do if(Input::isType("POST")){
  if(!Token::check(Input::get("token"))){
    $error = "CSRF detected";
    break;
  }

  $validation = new Validation($_POST, array(

    "id" => array(
      "required" => true
    ),
    "username" => array(
      "required" => true
    ),
    "email" => array(
      "required" => true
    )
  ));

  if(!$validation->passed()){
    $error = implode(', ', $validation->getErrors());
    break;
  }

  if(!$user->ChangeUsername(Input::get('username'))){
    $error .= "Changing username failed. ";
    break;
  }

  $success .= (empty($error)) ? "Username changed successfully. " : "";

  if(!$user->ChangeEmail(Input::get('email'))){
    $error .= "Changing email failed";
    break;
  }

  $success .= (empty($error)) ? "Email changed successfully. " : "";

} while(false);


 $success = (!empty($success)) ? "<div class=\"msg msg-ok\">
 			<p><strong>{$success}</strong></p>
 			<a href=\"#\" class=\"close\">close</a>
 		</div>" : "";

 $error = (!empty($error)) ? "<div class=\"msg msg-error\">
   <p><strong>{$error}</strong></p>
   <a href=\"#\" class=\"close\">close</a>
 </div>" : "";

$template = new Template("admin_user_edit");

$template->add("token", Token::generate());
$template->add("success", $success);
$template->add("error", $error);

$template->add("id", Input::get("id"));
$template->add("username", $user->getData()->username);
$template->add("email", $user->getData()->email);

$template->render();
