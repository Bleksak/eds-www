<?php

require_once '__core.php';

$login = (Session::exists("login")) ? Session::flash("login") : "";

$user = new User();

$template = new Template("index");
$template->add("navbar", $template->MakeNavbar());
$template->add("sidebar", $template->MakeSidebar());
$template->add("login", $login);
$template->add("articles", $template->MakeArticles(null, Input::get("page")));

$template->render();
