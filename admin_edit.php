<?php
require_once '__core.php';

$user = new User();
if(!$user->isLoggedIn() || !$user->isAdmin())
 Header::Redirect('/');

 $success = "";
 $error = "";

$articles = new Articles();

do if(Input::isType("POST")){

  if(!Token::check(Input::get("token"))){
    $error = "CSRF detected";
    break;
  }

  $validation = new Validation($_POST, array(
    "id" => array(
      "required" => true
    ),
    "Title" => array(
      "required" => true
    ),
    "Content" => array(
      "required" => true
    )
  ));

  if(!$validation->passed()){
    $error = implode(', ', $validation->getErrors());
    break;
  }

  if($articles->edit(Input::get("id"), Input::get("Title"), Input::get("Content"))){
    $success = "Edited successfully.";
    break;
  }
  $error = "There was an error editing.";

} while(false);

if($articles->find(Input::get("id")))
  $article = $articles->getResult();

else {
  $error = "Article ID {$article} does not exist.";
  Header::Redirect("/admin.php");
}



$success = (!empty($success)) ? "<div class=\"msg msg-ok\">
			<p><strong>{$success}</strong></p>
			<a href=\"#\" class=\"close\">close</a>
		</div>" : "";

$error = (!empty($error)) ? "<div class=\"msg msg-error\">
  <p><strong>{$error}</strong></p>
  <a href=\"#\" class=\"close\">close</a>
</div>" : "";

$template = new Template("admin_edit");

$template->add("id", Input::get("id"));
$template->add("token", Token::generate());
$template->add("success", $success);
$template->add("error", $error);

$template->add("title", $article->name);
$template->add("content", $article->content);

$template->render();
