<?php
require_once '__core.php';
$template = new Template("projects");
$template->add("navbar", $template->MakeNavbar());
$template->add("sidebar", $template->MakeSidebar());
$template->add("projects", $template->MakeProjects(null, Input::get("page")));

$template->render();
