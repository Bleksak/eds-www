<?php
require_once '__core.php';
$user = new User();

if(!Input::isType("POST") && !$user->isLoggedIn())
  Header::Redirect("/");

$message = ($user->login(Input::get("Username"), Input::get("Password"), !empty(Input::get("remember")))) ? "Logged-in successfully." : (Session::exists("banned") ? Session::flash("banned") : "Username or password is incorrect.");
Session::flash("login", $message);
Header::Redirect('/');
