<?php
require_once '__core.php';

if(!Input::exists("id"))
    Header::Redirect("/404.php");

$template = new Template("project");
$template->add("navbar", $template->MakeNavbar());
$template->add("sidebar", $template->MakeSidebar());


$template->add("project", $template->MakeProjects(Input::get("id")));

$template->render();
