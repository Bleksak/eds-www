 <?php
 require_once '__core.php';

$user = new User();
if(!$user->isLoggedIn() || !$user->isAdmin())
  Header::Redirect('/');

$error = "";
$success = "";

$articles = new Projects();

do if(Input::isType("POST")){

  //Add article

  if(!Token::check(Input::get("token"))){
    $error = "CSRF detected";
    break;
  }

  $validation = new Validation($_POST, array(
    "Title" => array(
      "required" => true
    ),

    "Content" => array(
      "required" => true
    )
  ));

  if(!$validation->passed()){
    $error = implode(', ', $validation->getErrors());
    break;
  }

  if($articles->create(Input::get("Title"), Input::get("Content"))){
    $success = "Project created successfully.";
    break;
  }

  $error = "There was an error creating article.";

} while (false);

if(Input::isType("GET") && Input::exists("del")){
  if($articles->delete(Input::get("del")))
    $success = "Project ". Input::get("del") . " successfully deleted.";
  else
    $error = "Project ". Input::get("del"). " couldn't be deleted.";
}

$currentPage = $articles->GetCurrentPage("page");

$articles->find(null, Articles::CalculateLimit($currentPage));

$articles_html = "";

$all = $articles->count();

$start =  $articles->getStart();

$end = $articles->getEnd();

$numberOfPages = $articles->GetNumberOfPages();

$previous = ($articles->isPrev()) ? "<a href='admin_projects.php?page=". (string)($currentPage-1) ."'>Previous</a>" : "";
$next = ($articles->isNext()) ? "<a href='admin_projects.php?page=". (string)($currentPage+1) ."'>Next</a>" : "";

$before = "";

for($i = 1; $i -1!=$articles->GetBeforeCount(); $i++){
  $before = "<a href='admin_projects.php?page=". (string)($currentPage -$i) ."'>". (string)($currentPage - $i) ."</a>" . $before;
}

$after = "";

for($i = 1; $i-1!=$articles->GetAfterCount(); $i++){
  $after .= "<a href='admin_projects.php?page=". (string)($currentPage + $i) ."'>". (string)($currentPage + $i) ."</a>";
}

$first = ($articles->isFirst()) ? "<a style='margin-right:20px;' href='admin.php?page=1'>1</a> " : "";
$last = ($articles->isLast()) ? "<a style='margin-left: 20px;' href='admin.php?page=" . $numberOfPages . "'>{$numberOfPages}</a>" : "";

if($all == 0){

  $articles_html = "<tr><td>There are no projects yet, go and add one!</td></tr>";

} else foreach($articles->getResult() as $article){
  $articles_html .= "<tr>
      <td><input type=\"checkbox\" class=\"checkbox\" /></td>
      <td><h3><a href='/project.php?id={$article->id}'>{$article->name}</a></h3></td>
      <td>{$article->date_added}</td>
      <td><a href='#'>{$article->author}</a></td>
      <td><a href='admin_projects.php?del={$article->id}' class=\"ico del\">Delete</a><a href='/admin_project_edit.php?id={$article->id}' class=\"ico edit\">Edit</a></td>
    </tr>";
}

$success = (!empty($success)) ? "<div class=\"msg msg-ok\">
			<p><strong>{$success}</strong></p>
			<a href=\"#\" class=\"close\">close</a>
		</div>" : "";

$error = (!empty($error)) ? "<div class=\"msg msg-error\">
  <p><strong>{$error}</strong></p>
  <a href=\"#\" class=\"close\">close</a>
</div>" : "";

$articles_data = "Showing {$start} - {$end} of {$all}";

$template = new Template("admin_projects");
$template->add("articles", $articles_html);
$template->add("articles_data", $articles_data);
$template->add("previous", $previous);
$template->add("next", $next);
$template->add("first", $first);
$template->add("last", $last);
$template->add("before", $before);
$template->add("after", $after);
$template->add("current", "<a class='active' href=\"admin.php?page=" . $currentPage . "\">{$currentPage}</a>");

$template->add("token", Token::generate());
$template->add("success", $success);
$template->add("error", $error);

$template->render();
