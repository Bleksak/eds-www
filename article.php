<?php
require_once '__core.php';
$user = new User();

$errors = "";

do if(Input::isType("POST")){

    if(!Token::check(Input::get("token"))){
        $error = "CSRF detected";
        break;
    }    

    $validation = new Validation($_POST, array(

        "id" => array(
            "required" => true
        ),
        "Comment" => array(
            "required" => true
        )
    ));

    if(!$validation->passed()){
        $errors .= implode("<br />", $validation->getErrors()) . "<br />";
        break;
    }

    $article = new Articles(Input::get("id"));

    if($article->addComment(htmlspecialchars(Input::get("Comment")))){
        $errors .= "Successfully added a comment.";
        break;
    }

    $errors .= "There was a problem adding a comment.";

} while (false);

echo $errors;

$template = new Template("article");
$template->add("navbar", $template->MakeNavbar());
$template->add("sidebar", $template->MakeSidebar());
$template->add("article", $template->MakeArticles(Input::get("id")));

$template->add("comments", nl2br($template->MakeComments(Input::get("id"))));


$template->add("addcomment", ($user->isLoggedIn()) ? (string) new Template("addcomment") : "");
$template->add("token", Token::generate());
$template->add("id", Input::get("id"));
$template->render();
